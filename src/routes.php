<?php
// Routes

$app->get('/', function ($request, $response, $args) {

    //
    // persons data is naturally better suited for a database or similar filterable storage
    // but I will use sessions here as a stand in, just for the technical test, as storage
    // does not appear to be part of the test
    //
    if(empty($_SESSION['persons'])){
        $_SESSION['persons'] = require ("./src/data.php");
    }

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/persons', function ($request, $response, $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/person', function ($request, $response, $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/about', function ($request, $response, $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/new', function ($request, $response, $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/templates/persons', function ($request, $response, $args) {
    return $this->renderer->render($response, 'ng/persons.phtml', $args);
});

$app->get('/templates/person', function ($request, $response, $args) {
    return $this->renderer->render($response, 'ng/person.phtml', $args);
});

$app->get('/templates/new', function ($request, $response, $args) {
    return $this->renderer->render($response, 'ng/new.phtml', $args);
});

$app->get('/templates/about', function ($request, $response, $args) {
    return $this->renderer->render($response, 'ng/about.phtml', $args);
});


$app->get('/data/person', function ($request, $response, $args) {
    
    $response = $response->withHeader('Content-type', 'application/json');
    $data = $_SESSION['persons'];
    
    $selected = $request->getQueryParam('guid');

    if( !empty($selected) ){
        $data = array_filter($data, function($personDetails) use ($selected) {
            if (isset($personDetails["guid"])) {
                if($personDetails["guid"] == $selected){
                    return true;        
                }
            }
            return false;
        });
    }

    $response = $response->withJson(array_shift($data));
    return $response;
});

$app->post('/data/new', function ($request, $response, $args) {
    $response = $response->withHeader('Content-type', 'application/json');
    $data = $_SESSION['persons'];

    $person = json_decode($request->getBody(), true);

    if( !empty($person) ){
        $person['guid'] = uniqid().'-'.uniqid();
        $person['picture'] = "http://placehold.it/32x32";
        $person['isActive'] = true;
        $data[] = $person;
        $_SESSION['persons'] = $data;
    }

    $response = $response->withJson($person);
    return $response;
});

$app->post('/data/persons', function ($request, $response, $args) {
    $response = $response->withHeader('Content-type', 'application/json');
    $data = $_SESSION['persons'];

    $selected = $request->getParam('eyeColor');

    if( !empty($selected) ){
        $data = array_filter($data, function($personDetails) use ($selected) {
            if (isset($personDetails["eyeColor"])) {
                if($personDetails["eyeColor"] == $selected){
                    return true;        
                }
            }
            return false;
        });
    }

    $search = $request->getParam('q');
    $fieldCheck = ["name", "address", "age"];

    if( !empty($search) ){
        $data = array_filter($data, function($personDetails) use ($search, $fieldCheck) {

            foreach($fieldCheck as $field){
                if( !empty($personDetails[$field]) && stripos($personDetails[$field], $search) !== false ){
                    return true; 
                }
            }

            return false;
        });
    }

    usort($data, function ($a, $b)
    {
        return strcmp($a["name"], $b["name"]);
    });
    
    $response = $response->withJson($data);
    return $response;
});

$app->get('/data/persons/friends', function ($request, $response, $args) {
    $response = $response->withHeader('Content-type', 'application/json');
    $data = $_SESSION['persons'];

    $selected = $request->getQueryParam('personGuid');

    if( !empty($selected) ){
        $data = array_filter($data, function($personDetails) use ($selected) {
            if (isset($personDetails["guid"])) {
                if($personDetails["guid"] == $selected){
                    return true;        
                }
            }
            return false;
        });
    }

    $friends = [];

    if( !empty($data) ){
        foreach($data as $person){
            $friends[$person['guid']] = $person['friends'];
        }
    }

    
    $response = $response->withJson($friends);
    return $response;
});
