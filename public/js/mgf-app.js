//
// Normally this code would be split in differnet files and concated into one file with grunt or equivalent
// for ease of code review it is presented in one file
//

// app-core.js
angular.module("mgf-app", ['ngRoute', 'ui.bootstrap']).config( function ($locationProvider, $routeProvider) {
        $locationProvider.html5Mode(true);
        $routeProvider.otherwise({
            redirectTo: '/persons'
        });
});

// controllers/persons.js
angular.module("mgf-app").controller("PersonsController", PersonsController);

angular.module("mgf-app").config( function($routeProvider, $sceDelegateProvider){
    $routeProvider.when('/persons', {
        templateUrl: '/templates/persons',
        controller: 'PersonsController',
        controllerAs: 'personsCtrl',
    });
  $sceDelegateProvider.resourceUrlWhitelist([
    'self',
    'https://www.google.com/maps/**'
  ]);
});

PersonsController.$inject = ['$routeParams', '$http', '$uibModal', '$location'];
function PersonsController($routeParams, $http, $uibModal, $location) {
    var ctrl = this;
    ctrl.debug = "If you can see this, then PersonsController is working :-)";
    ctrl.persons = [];
    ctrl.loaded = false;
    ctrl.eyeColor = $routeParams.eyeColor;
    ctrl.q = $routeParams.q;

    ctrl.search = function(){

        var params = {};

        if(ctrl.eyeColor != '')
            params.eyeColor = ctrl.eyeColor;

        if(ctrl.q != '')
            params.q = ctrl.q;

        $location.path('/persons').search(params);
    }

    ctrl.map = function(person){
        var modalInstance = $uibModal.open({
            component: 'mapModalComponent',
            resolve: {
               person: person
            }
        });

        modalInstance.result.then(function success(selectedItem) {

        }, function error(data) {
            console.warn('rejected modal : ', data);
        });
    }

    activate();

    function activate() {
        $http({
            method: 'POST',
            url: '/data/persons',
            data: $routeParams
        }).then(function success(response) {
            ctrl.loaded = true;
            ctrl.persons = response.data;
        }, function error(response) {
            ctrl.loaded = true;
            console.error('PersonsController activate error : ' , response);
        });
    }
};

angular.module('mgf-app').component('mapModalComponent', {
  templateUrl: 'myModalContent.html',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: function () {
    var $ctrl = this;
    $ctrl.opened = false;
    $ctrl.mapSrc = "";

    $ctrl.$onInit = function () {
      $ctrl.person = $ctrl.resolve.person;
       $ctrl.mapSrc = "https://www.google.com/maps/embed/v1/place?key=AIzaSyDcipeS7UEKJmzCkbq_Bw7WbzEl4lfOcFk&q="+$ctrl.person.latitude+","+$ctrl.person.longitude;
      
      $ctrl.opened = true;
    };

    $ctrl.ok = function () {
      $ctrl.close({$value: ''});
      $ctrl.opened = false;
    };

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
      $ctrl.opened = false;
    };
  }
});


// controllers/person.js
angular.module("mgf-app").controller("PersonController", PersonController);

angular.module("mgf-app").config( function($routeProvider){
    $routeProvider.when('/person', {
        templateUrl: '/templates/person',
        controller: 'PersonController',
        controllerAs: 'personCtrl',
    });
});
PersonController.$inject = ['$routeParams', '$http'];
function PersonController($routeParams, $http) {
    var ctrl = this;
    ctrl.debug = "If you can see this, then PersonController is working :-)";
    ctrl.person = {};
    ctrl.loaded = false;

    activate();

    function activate() {
        $http({
            method: 'GET',
            url: '/data/person?guid=' + $routeParams.guid
        }).then(function success(response) {
            ctrl.loaded = true;
            ctrl.person = response.data;
        }, function error(response) {
            ctrl.loaded = true;
            console.error('PersonsController activate error : ' , response);
        });
    }
};


// controllers/about.js
angular.module("mgf-app").controller("AboutController", AboutController);

angular.module("mgf-app").config( function($routeProvider){
    $routeProvider.when('/about', {
        templateUrl: '/templates/about',
        controller: 'AboutController',
        controllerAs: 'aboutCtrl',
    });
});

AboutController.$inject = ['$routeParams'];
function AboutController($routeParams) {
    var ctrl = this;
    ctrl.debug = "If you can see this, then AboutController is working :-)";

    activate();

    function activate() {
        console.info('AboutController called');
    }
};

// controllers/persons.js
angular.module("mgf-app").controller("NewController", NewController);

angular.module("mgf-app").config( function($routeProvider, $sceDelegateProvider){
    $routeProvider.when('/new', {
        templateUrl: '/templates/new',
        controller: 'NewController',
        controllerAs: 'newCtrl',
    });
});

NewController.$inject = ['$routeParams', '$http', '$window', '$location'];
function NewController($routeParams, $http, $window, $location) {
    var ctrl = this;
    ctrl.debug = "If you can see this, then NewController is working :-)";
    ctrl.person = {};

    ctrl.submit = function(){
        $http({
            method: 'POST',
            url: '/data/new',
            data: ctrl.person
        }).then(function success(response) {
             $location.path('/persons').search({q: response.data.name});
        }, function error(response) {
            ctrl.loaded = true;
            console.error('PersonsController activate error : ' , response);
        });
    }
}




// filters/address.js

angular.module('mgf-app').filter("formatAddress", FormatAddress);

function FormatAddress() {
    return function(input) {
        if (input != null)
            return input.split(", ").join(",\n");
    }
}